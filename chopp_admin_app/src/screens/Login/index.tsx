import { SafeAreaView } from "react-native-safe-area-context";
import { View } from 'react-native' 
import Input from '../../components/Input'
import Button from '../../components/Button/index'
import { useContext, useState } from "react";

export default function Login() {
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    
    const tryLogin = () => {
        // Tries to make a request to our login endpoint
        
    }

    return(
        <SafeAreaView>
            <View>
                <Input content={email} setContent={setEmail} hint="Insira seu email"/>
                <Input content={password} setContent={setPassword} hint="Insira sua senha"/>
                <Button action={tryLogin}>Entrar</Button>
            </View>
        </SafeAreaView>
    )
}