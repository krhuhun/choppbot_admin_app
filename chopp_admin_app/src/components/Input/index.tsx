import { TextInput } from "react-native";

interface InputProps {
    content: string,
    setContent: any,
    hint: string,
    //keyboardType: string, // deafult, numeric, phone-pad, url, visible-password, decimal-pad
}

export default function Input({ content, setContent, hint, }: InputProps) {
    return(
        <TextInput 
            placeholder={hint}
            value={content}
            onChangeText={setContent}
        />
    )
}