import { TouchableOpacity, Text } from "react-native";

interface ButtonProps {
    action: any,
    children: String,
}

export default function Button({ action, children }: ButtonProps) {
    return(
        <TouchableOpacity onPress={action}>
            <Text>{children}</Text>
        </TouchableOpacity>
    )
}